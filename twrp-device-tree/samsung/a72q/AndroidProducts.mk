#
# Copyright (C) 2021 The Android Open Source Project
# Copyright (C) 2021 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/omni_a72q.mk

COMMON_LUNCH_CHOICES := \
    omni_a72q-user \
    omni_a72q-userdebug \
    omni_a72q-eng
